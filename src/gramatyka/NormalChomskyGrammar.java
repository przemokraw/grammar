package gramatyka;

import java.util.HashSet;
import java.util.ListIterator;
import java.util.Set;


/**
 * Klasa do reprezentacji gramatyk w postaci normalnej Chomsky'ego. Zawiera dodatkową metodę,
 * która umożliwia konwersję do gramatyki w postaci normalne Greibach.
 * @author przemek
 *
 */
public class NormalChomskyGrammar extends ContextFreeGrammar
{

	/**
	 * Tworzy obiekt klasy NormalChomskyGrammmar na podstawie istniejącego
	 * obiektu ContextFreeGrammar
	 * @param g obiekt ContextFreeGrammar, na podstawie którego tworzymy
	 */
	public NormalChomskyGrammar(ContextFreeGrammar g) {
		super(g);
		if(!ifChomsky())
		{
			System.out.println("Gramatyka podana jako parametr nie jest gramatyką w postaci normalnej Chomsky'ego.");
			System.exit(1);
		}

	}

	
	@Override
	protected String form() {
		return "Chomsky";
	}
	
	
	/**
	 * Konweruje gramatykę w postaci normalnej Chomsky'ego do gramatyki w postaci
	 * normalnej Greibach. Obiekt, na którym wywołano metodę pozostaje bez zmian.
	 * Skrócony opis działania:
	 * <ol>
	 * 	<li>Pobranie z obiektu, na którym wywołano metodę informacji o terminalach, nieterminalach i produkcjach</li>
	 * 	<li>Poprawienie prawych stron produkcji tak, aby pierwszy ich symbol był co najmniej taki, jak symbol po lewej stronie (na symbole nałożony porządek)</li>
	 * 	<li>Poprawienie prawych stron, w których pierwszy sybmol był taki sam jak symbol po lewej stronie</li>
	 * 		<ul>
	 * 			<li> Dla każdego nieteminala utworzony zostanie nowa pomocnicza produkca, w której jako lewa strona będzie zawsze wybrana pierwsza wolna wielka litera alfabetu (w kolejności alfabetycznej).
	 * 			<li> Utworzona pomocnicza produkcja zostanie dodana na początek istniejącej już listy produkcji.
	 * 		</ul>
	 * 	<li>Poprawienie prawych stron tak, żeby zaczynały się od terminala</li>
	 * 	<li>Utworzenie nowego obiektu klasy NormalGreibachGrammar o parametrach uzyskanych w wyniku działania metody</li>
	 *</ol>
	 * @return nowy obiekt klasy NormalGreibachGrammar odpowiadający gramatyce na której wywołano metodę.
	 */
	public NormalGreibachGrammar toGreibach()
	{
		/* terminale nie będą ruszane, więc wystarzczy referencja z poprzedniej gramatyki */
		Alphabet<Terminal> terminalsForNewGrammar = this.getTerminals();
		/* w nowej gramatyce mogą dojść nowe terminale, referencja nie wystarczy */
		Alphabet<Nonterminal> nonterminalsForNewGrammar = new Alphabet<Nonterminal>();
		for(Nonterminal n: this.getNonterminals())
		{
			try
			{	
				nonterminalsForNewGrammar.addSymbolToEnd(n);
			}
			catch(DuplicateSymbol d){ /*obiekt NormalChomskyGrammar powstał z ContextFreeGrammar, w którtym obsłużono ten wyjątek */}
		}
		/* produkcje w nowej gramatyce również ulegnie zmianie, więc referencja nie wystarczy */
		ProductionsSet productionsForNewGrammar = new ProductionsSet(
				nonterminalsForNewGrammar,
				terminalsForNewGrammar,
				this.getProductionsSet().toArrayTable()
				);
		
		/* zbiór, w który przechowuje już poprawione nieterminale */
		Set<Nonterminal> alreadyCorrectedNonterminals = new HashSet<Nonterminal>();
		
		
		/* 1) poprawiam produkcje, w których piewszy symbol po prawej stronie
		 * jest nieterminalem wcześniej poprawionym.
		 */
		/* na razie nieterminale w obu gramtykach są takie same,
		 * więc iteruję po nieterminalach z this, żeby nieterminale
		 * nonterminalsForNewGrammar móc edytować. 
		 */
		for(Nonterminal n: this.getNonterminals())
		{
			/* Poprawienie prawych stron produkcji tak, aby pierwszy ich symbol był co najmniej taki, jak symbol po lewej stronie */
			WordList newRightHandSideOfProduction = new WordList();
			for(SymbolList word: productionsForNewGrammar.getRightHandSide(n))
			{
				Symbol firstSymbol = word.getFirstSymbol();
				if(!firstSymbol.isTerminal() && alreadyCorrectedNonterminals.contains(firstSymbol))
				{
					WordList rhsForFirstSymbol = productionsForNewGrammar.getRightHandSide(nonterminalsForNewGrammar.getSymbol(firstSymbol.getName()));
					newRightHandSideOfProduction.addWordList(rhsForFirstSymbol.doProduction(word));
				}
				else
				{
					newRightHandSideOfProduction.addWord(word);
				}
			}
			productionsForNewGrammar.setRightHandSide(n, newRightHandSideOfProduction);

			/* Poprawienie prawych stron, w których pierwszy sybmol był taki sam jak symbol po lewej stronie (produkcji rekursywnych)*/
			int numberOfRecursiveProductions = 0;
			for(SymbolList word: productionsForNewGrammar.getRightHandSide(n))
				if(word.getFirstSymbol().equals(n))
					numberOfRecursiveProductions++;
			if(numberOfRecursiveProductions > 0)
			{
				newRightHandSideOfProduction = new WordList();
				Nonterminal lhsForNewProduction = null;
				try
				{
					lhsForNewProduction = productionsForNewGrammar.createNewProduction();
				}
				catch(LackOfAvailableSymbols l)
				{
					System.out.println("Błąd. toGreibach(): nie udało się stworzyć nowego nieterminala, brak dostępnych symboli. Zamykam program.");
					System.exit(1);
				}
				for(SymbolList word: productionsForNewGrammar.getRightHandSide(n))
				{
					Symbol firstSymbol = word.getFirstSymbol();
					if(firstSymbol.equals(n))
					{	
						productionsForNewGrammar.addOne(lhsForNewProduction, word.getAllExceptFirst());
						productionsForNewGrammar.addOne(lhsForNewProduction, word.getAllExceptFirst().merge(new SymbolList(lhsForNewProduction)));
					}
					else
					{
						newRightHandSideOfProduction.addWord(word);
						newRightHandSideOfProduction.addWord(word.merge(new SymbolList(lhsForNewProduction)));
					}
				}
				productionsForNewGrammar.setRightHandSide(n, newRightHandSideOfProduction);
			}
			alreadyCorrectedNonterminals.add(n);
		}
		
		/* 2) poprawienie produkcji tak, by na początku każdej prawej strony był terminal. */
		ListIterator<Nonterminal> reverseIt = nonterminalsForNewGrammar.reverseIterator();
		while(reverseIt.hasPrevious())
		{
			Nonterminal lhs = reverseIt.previous();
			WordList newRightHandSideOfProduction = new WordList();
			for(SymbolList word: productionsForNewGrammar.getRightHandSide(lhs))
			{
				if(!word.getFirstSymbol().isTerminal())
				{
					WordList rhsForFirstSymbol = productionsForNewGrammar.getRightHandSide(nonterminalsForNewGrammar.getSymbol(word.getFirstSymbol().getName()));
					newRightHandSideOfProduction.addWordList(rhsForFirstSymbol.doProduction(word));	
				}
				else
				{
					newRightHandSideOfProduction.addWord(word);
				}
			}
			productionsForNewGrammar.setRightHandSide(lhs, newRightHandSideOfProduction);
			
		}
		
		return new NormalGreibachGrammar(new ContextFreeGrammar(terminalsForNewGrammar, nonterminalsForNewGrammar, productionsForNewGrammar));
	}
}
