package gramatyka;

/**
 * Klasa opisująca symbol będący terminalem
 * @author przemek
 *
 */
public class Terminal extends Symbol
{

	/**
	 * Konstruktor dla klasy Terminal. Z definicji terminal
	 * opisany jest małą literą alfabetu angielskiego, dlatego
	 * konstruktor rzuca wyjątek ForbiddenChar, jeśli do jego
	 * stworzenia użyje się innego znaku
	 * @param name mała litera alfabetu angielskiego
	 * @throws ForbiddenChar rzucany, jeśli do stworzenia użyto innego znaku, niża mała litera
	 * alfabetu angielskiego
	 */
	public Terminal(Character name) throws ForbiddenChar
	{
		super(name);
		
		if(!name.toString().matches("[a-z]"))
			throw new ForbiddenChar();	
	}
	
	@Override
	public boolean isTerminal() {
		return true;
	}

}
