package gramatyka;

/**
 * Klasa reprezentująca gramatykę bezkontekstową.
 * @author przemek
 *
 */
public class ContextFreeGrammar
{
	private final Alphabet<Nonterminal> nonterminals;
	private final Alphabet<Terminal> terminals;
	private final ProductionsSet productions;
	
	public ContextFreeGrammar(
			String terminalSymbols,
			String nonterminalSymbols,
			String [][] productionArray)
	{
		nonterminals = new Alphabet<Nonterminal>();
		terminals = new Alphabet<Terminal>();
		/* stworzenie terminali */
		for(int i = 0; i < terminalSymbols.length(); i++)
		{
			try
			{
				Terminal t = new Terminal(terminalSymbols.charAt(i));
				terminals.addSymbolToEnd(t);
			}
			catch(ForbiddenChar f)
			{
				System.out.println("Terminal: niedozwolony znak " + terminalSymbols.charAt(i) + ". Zamykam program.");
				System.exit(1);
			}
			catch(DuplicateSymbol d)
			{
				System.out.println("Terminal: zduplikowany symbol " + terminalSymbols.charAt(i) + ". Zamykam program.");
				System.exit(1);				
			}
		}
		
		/* stworzenie nieterminali */
		for(int i = 0; i < nonterminalSymbols.length(); i++)
		{
			try
			{					
				Nonterminal n = new Nonterminal(nonterminalSymbols.charAt(i));
				nonterminals.addSymbolToEnd(n);
			}
			catch(ForbiddenChar f)
			{
				System.out.println("Nieterminal: niedozwolony znak " + nonterminalSymbols.charAt(i) + ". Zamykam program.");
				System.exit(1);
			}
			catch(DuplicateSymbol d)
			{
				System.out.println("Nonterminal: zduplikowany symbol " + nonterminalSymbols.charAt(i) + ". Zamykam program.");
				System.exit(1);				
			}
		}
		
		/* stworzenie produkcji */
		productions = new ProductionsSet(nonterminals, terminals, productionArray);	
	}
	
	protected ContextFreeGrammar(Alphabet<Terminal> terminals, Alphabet<Nonterminal> nonterminals, ProductionsSet productions)
	{
		this.terminals = terminals;
		this.nonterminals = nonterminals;
		this.productions = productions;
	}
	
	protected ContextFreeGrammar(ContextFreeGrammar g)
	{
		this.nonterminals = g.nonterminals;
		this.terminals = g.terminals;
		this.productions = g.productions;
	}
	
	protected Alphabet<Terminal> getTerminals()
	{
		return terminals;
	}
	
	protected Alphabet<Nonterminal> getNonterminals()
	{
		return nonterminals;
	}
	
	protected ProductionsSet getProductionsSet()
	{
		return productions;
	}
	
	/* dodatkowe, prywatne funkcje do sprawdzania odpowiednio lewo- i prawo- liniowości.
	 * Wykorzystane w fukcji ifRegular().
	 */
	private boolean ifLeftRegular() {
		for(Nonterminal n: nonterminals) {
			WordList rightHandSideOfProduction = productions.getRightHandSide(n);
			for(SymbolList word: rightHandSideOfProduction) {
				if(!word.isLeftLinearRegularForm())
					return false;
			}
		}
		return true;		
	}
	
	private boolean ifRightRegular() {
		for(Nonterminal n: nonterminals) {
			WordList rightHandSideOfProduction = productions.getRightHandSide(n);
			for(SymbolList word: rightHandSideOfProduction) {
				if(!word.isRightLinearRegularForm())
					return false;
			}
		}
		return true;		
	}
	
	/**
	 * Sprawdza, czy gramatyka bezkontekstowa jest regularna.
	 * @return true, jeśli regularna; false w p.p.
	 */
	public boolean ifRegular() {
		if(ifLeftRegular())
			return true;
		else if(ifRightRegular())
			return true;
		else
			return false;
	}
	
	/**
	 * Sprawdza, czy gramatyka bezkontekstowa jest w postaci normalniej Chomsky'ego.
	 * @return true, jeśli jest w postaci normalniej Chomsky'ego; false w p.p. 
	 */
	public boolean ifChomsky() {
		for(Nonterminal n: nonterminals) {
			WordList rightHandSideOfProduction = productions.getRightHandSide(n);
			for(SymbolList word: rightHandSideOfProduction) {
				if(!word.isChomskyNormalForm())
					return false;
			}
		}
		return true;
	}
	
	/**
	 * Sprawdza, czy gramatyka bezkontekstowa jest w postaci normalnej Greibach.
	 * @return true, jeśli jest w postaci normalnej Greibach.; false w p.p.
	 */
	public boolean ifGreibach() {
		for(Nonterminal n: nonterminals) {
			WordList rightHandSideOfProduction = productions.getRightHandSide(n);
			for(SymbolList word: rightHandSideOfProduction) {
				if(!word.isGreibachNormalForm())
					return false;
			}
		}
		return true;		
	}
	
	/**
	 * Wypisuje typ gramatyki.
	 * @return napis - typ gramatyki.
	 */
	protected String type() {
			return "bezkontekstowa";
	}	
	/* a może powinno być tak?:
	protected String type() {
		if(ifRegular())
			return "regularna";
		else
			return "bezkontekstowa";
	}
	 */
	
	/**
	 * Wypisuje formę gramatyki.
	 * @return pusty string, jeśli gramatyka nie jest ani w postaci Chomsky'ego ani Greibach,
	 * napis "Chomsky" lub "Greibach", jeśli grmatyka jest jednego z podancyh typów.
	 */
	protected String form() {
		if(ifChomsky())
			return "Chomsky";
		else if(ifGreibach())
			return "Greibach";
		else
			return "";
	}
	
	@Override
	public String toString() {
		String res = "Gramatyka: " + type() + "/" + form() + "\nTerminale: ";
		res += terminals.toString();
		res += "\nNieterminale: ";
		res += nonterminals.toString();
		res += "\nProdukcje:\n";
		res += productions.toString();

		return res;
	}
	

}
