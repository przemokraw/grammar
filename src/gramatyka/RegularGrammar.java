package gramatyka;

/**
 * Klasa reprezentująca gramatykę regularną.
 * @author przemek
 *
 */
public class RegularGrammar extends ContextFreeGrammar {
	
	/**
	 * Tworzy obiekt klasy RegularGrammar na podstawie istniejącego
	 * obiektu ContextFreeGrammar
	 * @param g obiekt ContextFreeGrammar, na podstawie którego tworzymy
	 */
	public RegularGrammar(ContextFreeGrammar g) {
		super(g);
		
		if(!ifRegular())
		{
			System.out.println("Gramatyka podana jako parametr nie jest gramatyką regularną. Zamykam program.");
			System.exit(1);
		}
	}
	
	@Override
	protected String type() {
		return "regularna";
	}
	
}
