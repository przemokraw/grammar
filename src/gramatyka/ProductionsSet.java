package gramatyka;

import java.util.HashMap;
import java.util.Map;

/**
 * Klasa reprezentująca zbiór produkcji gramatyki.
 * @author przemek
 *
 */
public class ProductionsSet
{
	private Alphabet<Nonterminal> lhs;
	private Map<Nonterminal, WordList> productions = 
			new HashMap<Nonterminal, WordList>();
	/**
	 * Konstruktor.
	 * @param nonterminals Alfabet nieterminali gramatyki
	 * @param terminals Alfabet terminali gramatyki
 	 * @param productions Tablica tablic przechowująca produkcje, sformatowana jak w treści zadania.
	 */
	public ProductionsSet(Alphabet<Nonterminal> nonterminals, Alphabet<Terminal> terminals, String [][] productions)
	{
		lhs = nonterminals;
		int i = 0;
		for(Nonterminal n: nonterminals)
		{
			Nonterminal leftHandSideOfProduction = n;
			WordList rightHandSideOfProduction = new WordList();
			
			for(String rhs: productions[i])
			{
				try
				{
					SymbolList word = new SymbolList(rhs, terminals, nonterminals);
					rightHandSideOfProduction.addWord(word);
				}
				catch(ForbiddenChar f)
				{
					System.out.println("W produkcji użyto niedozwolonego znaku. Zamykam program.");
					System.exit(1);
				}
			}
			this.productions.put(leftHandSideOfProduction, rightHandSideOfProduction);
			i++;
		}
	}
	
	public ProductionsSet() {}
	
	/**
	 * Ustawia prawą stronę zadanej produkcji.
	 * @param leftHandSide Lewa strona produkcji, której prawa strona będzie zmieniona.
	 * @param rightHandSide Ciąg słów reprezentujących nową prawą stronę produkcji.
	 */
	public void setRightHandSide(Nonterminal leftHandSide, WordList rightHandSide)
	{
		productions.put(leftHandSide, rightHandSide);
	}
	
	/**
	 * Zwraca prawą stronę produkcji.
	 * @param leftHandSide Lewa strona produkcji.
	 * @return Ciąg słów będący prawą stroną produkcji.
	 */
	public WordList getRightHandSide(Nonterminal leftHandSide)
	{
		return productions.get(leftHandSide);
	}
	
	/* Tworzy nowy nieterminal wykorzystująć pierwszy w kolejności alfabetycznej wolkny symbol */
	private Nonterminal createNewNonterminal() throws LackOfAvailableSymbols
	{
		for(char c = 'A'; c <= 'Z'; c++)
		{
			try
			{
				Nonterminal n = new Nonterminal(c);
				lhs.addSymbolToBegin(n);
				return n;
			}
			catch(ForbiddenChar f) {}
			catch(DuplicateSymbol d) {}
		}
		throw new LackOfAvailableSymbols();
	}
	
	/**
	 * Tworzy nową, pustą produkcję.
	 * @return Nieterminal będący lewą stroną stworzonej produkcji.
	 * @throws LackOfAvailableSymbols jeśli nie udało się stworzyć produkcji, ponieważ zabrakło symboli na nowy nieterminal.
	 */
	public Nonterminal createNewProduction() throws LackOfAvailableSymbols
	{
		try
		{
			Nonterminal newLhs = createNewNonterminal();
			productions.put(newLhs, new WordList());
			return newLhs;
		}
		catch(LackOfAvailableSymbols l)
		{
			throw new LackOfAvailableSymbols();
		}
	}
	
	/**
	 * Dodaje jedno słowo do prawej strony produkcji.
	 * @param leftHandSide Lewa strona produkcji, do której chcemy dodać słowo.
	 * @param rightHandSide Słowo do dodania.
	 */
	public void addOne(Nonterminal leftHandSide, SymbolList rightHandSide)
	{
		productions.get(leftHandSide).addWord(rightHandSide);
	}
	
	/**
	 * Konwertuje produkcję do tablicy tablic Stringów w takim formacie,
	 * jak w treści zadania.
	 * @return Tabilca tablic stringów reprezentujących zbiór produkcji.
	 */
	public String[][] toArrayTable()
	{
		String [][] array = new String[lhs.size()][];
		int i = 0;
		for(Nonterminal n: lhs)
		{
			WordList rhs = getRightHandSide(n);
			array[i] = new String[rhs.size()];
			int j = 0;
			for(SymbolList word: rhs)
			{
				array[i][j] = word.toString();
				j++;
			}
			i++;
		}
		return array;
	}
	
	@Override
	public String toString()
	{
		String res = "";
		for(Nonterminal n: lhs)
			for(SymbolList rhs: getRightHandSide(n))
				res += n.toString() + " -> " + rhs.toString() + "\n";
		return res;
	}

}

class LackOfAvailableSymbols extends Exception {
	private static final long serialVersionUID = 1L;}