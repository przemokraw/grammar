package gramatyka;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Klasa przechowująca ciąg słów. Wykorzystywana jako reprezentacja prawej strony produkcji.
 * @author przemek
 *
 */
public class WordList implements Iterable<SymbolList>
{
	List<SymbolList> words = new LinkedList<SymbolList>();
	
	/**
	 * Tworzy pusty ciąg słów.
	 */
	public WordList() {}
	
	/**
	 * Konstruktor kopiujący.
	 * @param other Ciąg słów do skopiowania.
	 */
	public WordList(WordList other)
	{
		for(SymbolList word: other)
			addWord(word);		
	}
	
	/**
	 * Wstawia słowo na koniec istniejącego ciągu słów, o ile to słowo
	 * nie występuje już w tym ciągu. Zapiobiega to dodawaniu takich samych produkcji
	 * podczas wykonywania algorytmu konwertującego gramatyki w postaci Chomsky'ego
	 * do gramatyk w postaci Greibach.
	 * @param word
	 */
	public void addWord(SymbolList word)
	{
		SymbolList ls = new SymbolList();
		ls = ls.merge(word);
		if(!words.contains(ls))
			words.add(ls);
		
	}
	
	/**
	 * Wstawia do ciągu słów wszystkie słowa z innego ciągu słów,
	 * których jeszcze nie ma w ciągu.
	 * @param other Ciąg słów do wstawienia.
	 */
	public void addWordList(WordList other)
	{
		for(SymbolList sl: other)
			addWord(sl);
	}

	public int size()
	{
		return words.size();
	}
	
	/**
	 * Tworzy wszystkie możliwe produkcje zamieniając pierwszy symbol
	 * słowa przekazanego jako parametr na słowa występujące w ciągu słów,
	 * na którym wywołano metodę, np. wywołanie metody z parametrem AA na ciągu
	 * słów AB, BC zwróci ciąg ABA, BCA.
	 * @param word Słowo, na którym dokonujemy produkcję.
	 * @return Ciąg słów będących rezultatem wykonywanej produkcji.
	 */
	public WordList doProduction(SymbolList word)
	{
		WordList produced = new WordList();
		for(SymbolList s: words)
		{
			produced.addWord(s.merge(word.getAllExceptFirst()));
		}
		return produced;
	}


	@Override
	public Iterator<SymbolList> iterator() {
		return words.listIterator();
	}
}
