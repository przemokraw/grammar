package gramatyka;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Klasa przechowująca słowo, czyli ciąg Symboli.
 * @author przemek
 *
 */
public class SymbolList implements Iterable<Symbol>
{

	private List<Symbol> symbols = new LinkedList<Symbol>();
	
	public SymbolList() {}
	
	public SymbolList(String word, Alphabet<Terminal> terminals, Alphabet<Nonterminal> nonterminals) throws ForbiddenChar
	{
		for(int i = 0; i < word.length(); i++)
		{
			char c = word.charAt(i);
			if(terminals.containsSymbol(c))
				symbols.add(terminals.getSymbol(c));
			else if(nonterminals.containsSymbol(c))
				symbols.add(nonterminals.getSymbol(c));
			else
				throw new ForbiddenChar();				
		}
	}
	
	/**
	 * Konstruktor tworzący słowo o długości 1 dla zadanego symbolu.
	 * @param s Symbol, który chcemy przedstawić jako słowo.
	 */
	protected SymbolList(Symbol s)
	{
		symbols.add(s);
	}
	
	public boolean isEmpty()
	{
		return symbols.isEmpty();
	}
	
	public int length()
	{
		return symbols.size();
	}

	public Symbol getFirstSymbol() 
	{
		return symbols.get(0);
	}
	
	public Symbol getSecondSymbol()
	{
		return symbols.get(1);
	}
	
	/**
	 * Ucina pierwszą literę słowa.
	 * @return Słowo, które jest podsłowem wyjściowego słowa bez pierwszego znaku.
	 */
	public SymbolList getAllExceptFirst()
	{
		if(symbols.size() == 0)
		{
			return null;
		}
		else if(symbols.size() == 1)
		{
			return new SymbolList();
		}
		else
		{
			SymbolList sl = new SymbolList();
			for(Symbol s: symbols.subList(1, symbols.size()))
				sl.addToEnd(s);
			return sl;
		}
			
	}
	
	public void addToBegin(Symbol s)
	{
		symbols.add(0, s);
	}
	
	public boolean contains(Symbol s)
	{
		return symbols.contains(s);
	}
	
	public void addToEnd(Symbol s)
	{
		symbols.add(s);
	}
	
	/**
	 * Dokleja słowo.
	 * @param other Słowo do doklejenia.
	 * @return Nowe słowo, które stanowi sklejenie słowa, na którym wywołano metodę i parametru.
	 */
	public SymbolList merge(SymbolList other)
	{
		SymbolList sl = new SymbolList();
		for(Symbol symb: this.symbols)
		{
			sl.addToEnd(symb);
		}
		for(Symbol symb: other)
		{
			sl.addToEnd(symb);
		}
		return sl;
	}
	
	@Override
	public Iterator<Symbol> iterator()
	{
		return symbols.listIterator();
	}
	
	@Override
	public String toString()
	{
		if(isEmpty())
		{
			return "&";
		}
		else
		{
			String res = "";
			for(Symbol s: symbols)
				res += s.toString();
			return res;
		}
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null)
			return false;
		else if(obj instanceof SymbolList)
			return this.toString().equals(((SymbolList)obj).toString());
		else
			return false;
	}
	
	/* Poniżej zdefiniowane będzie kilka metod, które umożliwią klasą z pakietu
	 * gramatyka sprawdzanie pewnych własności słowa. Będzie to przydatne przy
	 * sprawdzaniu typów i form grmatyk.
	 */
	
	/** 
	 * Zwraca true, jeśli słow jest albo puste, albo ma długość 1 i jest terminalem,
	 * albo ma długość 2 i jest postaci cD, gdzie c to terminal, a D to nieterminal
	 * @return true, jeśli któryś z powyższych warunków jest spełniony, false w p.p.
	 */
	protected boolean isLeftLinearRegularForm() {
		if(isEmpty() || ((length() == 1) && getFirstSymbol().isTerminal()))
			return true;
		else if((length() == 2) && getFirstSymbol().isTerminal() && !getSecondSymbol().isTerminal())
				return true;
		else
			return false;	
	}
	
	/** 
	 * Zwraca true, jeśli słow jest albo puste, albo ma długość 1 i jest terminalem,
	 * albo ma długość 2 i jest postaci Dc, gdzie c to terminal, a D to nieterminal
	 * @return true, jeśli któryś z powyższych warunków jest spełniony, false w p.p.
	 */
	protected boolean isRightLinearRegularForm() {
		if(isEmpty() || ((length() == 1) && getFirstSymbol().isTerminal()))
			return true;
		else if((length() == 2) && !getFirstSymbol().isTerminal() && getSecondSymbol().isTerminal())
				return true;
		else
			return false;
	
	}
	
	/**
	 * Sprawdza, czy słowo reprezentuje terminal, czyli jest długości 1 i jedyny symbol
	 * jest terminalem.
	 * @return true, jeśli słowo reprezentuje terminal; false w p.p.
	 */
	protected boolean isTerminal() {
		if((length() == 1) && (getFirstSymbol().isTerminal()))
			return true;
		else
			return false;
	}
	/**
	 * Zwraca true, jeśli słowo jest postaci a, gdzie a - terminal albo BC, gdzie B i C to
	 * nieterminale
	 * @return true, jeśli któryś z powyższych warunków jest spełniony, false w p.p.
	 */
	protected boolean isChomskyNormalForm() {
		if(isEmpty()) {
			return false;
		}
		else {
			if((length() == 1) && getFirstSymbol().isTerminal())
				return true;
			else if((length() == 2) && !getFirstSymbol().isTerminal() && !getSecondSymbol().isTerminal())
				return true;
			else
				return false;
		}
	}
	
	/**
	 * Zwraca true, jeśli słowo jest postaci aX, gdzie a - terminal, X - ciąg nieterminali
	 * @return true, jeśli któryś z powyższych warunków jest spełniony, false w p.p.
	 */
	protected boolean isGreibachNormalForm() {
		if(isEmpty()) {
			return false;
		}
		else if(!getFirstSymbol().isTerminal()) {
			return false;
		}
		else { //pierwszy symbol na pewno jest terminalem
			int numberOfTerminals = 0;
			for(Symbol s: symbols)
			{
				if(s.isTerminal())
					numberOfTerminals++;
				
				if(numberOfTerminals > 1)
					return false;
			}
			return true;
		}
	}
}
