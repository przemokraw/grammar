package gramatyka;

/**
 * Klasa opisująca symbol będący nieterminalem
 * @author przemek
 *
 */
public class Nonterminal extends Symbol
{

	/**
	 * Konstruktor dla klasy Nonterminal. Z definicji nieterminal
	 * opisany jest wielką literą alfabetu angielskiego, dlatego
	 * konstruktor rzuca wyjątek ForbiddenChar, jeśli do jego
	 * stworzenia użyje się innego znaku
	 * @param name wielka litera alfabetu angielskiego
	 * @throws ForbiddenChar rzucany, jeśli do stworzenia użyto innego znaku, niż wielka litera
	 * alfabetu angielskiego
	 */
	public Nonterminal(Character name) throws ForbiddenChar
	{
		super(name);
		
		if(!name.toString().matches("[A-Z]"))
			throw new ForbiddenChar();	
	}
	
	public Nonterminal(Symbol s) throws ForbiddenChar {
		super(s.getName());
		
		if(!s.toString().matches("[A-Z]"))
			throw new ForbiddenChar();	
	}
	
	@Override
	public boolean isTerminal() {
		return false;
	}

}
