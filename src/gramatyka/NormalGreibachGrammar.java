package gramatyka;


/**
 * Klasa do reprezentacji gramatyk w postaci normalnej Greibach.
 * @author przemek
 *
 */
public class NormalGreibachGrammar extends ContextFreeGrammar {
	
	/**
	 * Tworzy obiekt klasy NormalGreibachGrammar na podstawie istniejącego
	 * obiektu ContextFreeGrammar
	 * @param g obiekt ContextFreeGrammar, na podstawie którego tworzymy
	 */
	public NormalGreibachGrammar(ContextFreeGrammar g) {
		super(g);
		
		if(!ifGreibach())
		{
			System.out.println("Gramatyka podana jako parametr nie jest gramatyką w postaci Greibach. Zamykam program.");
			System.exit(1);
		}
	}

	@Override
	protected String form() {
		return "Greibach";
	} 
	
}