package gramatyka;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;


/**
 * Klasa przechowująca wykorzystywane przez gramatykę symbole.
 * Umożliwia między innymy sprawdzenie, czy symbol opisany danym
 * znakiem znajduje się w alfabecie.
 * @author przemek
 *
 */
public class Alphabet<T extends Symbol> implements Iterable<T>
{
	private List<T> symbols = new LinkedList<T>();
	
	/**
	 * Tworzy pusty alfabet.
	 */
	public Alphabet() {	}
	
	/**
	 * Wstawia symbol na poczętek alfabetu, jeśli wcześniej nie był wstawiony.
	 * @param s symbol do wstawienia.
	 * @throws DuplicateSymbol jeśli dany symbol już znajdował się w alfabecie.
	 */
	public void addSymbolToBegin(T s) throws DuplicateSymbol
	{
		if(!symbols.contains(s))
			symbols.add(0,s);
		else
			throw new DuplicateSymbol();
	}
	
	/**
	 * Wstawia symbol na koniec alfabetu, jeśli wcześniej nie był wstawiony.
	 * @param s symbol do wstawienia.
	 * @throws DuplicateSymbol jeśli dany symbol już znajdował się w alfabecie.
	 */
	public void addSymbolToEnd(T s) throws DuplicateSymbol
	{
		if(!symbols.contains(s))
			symbols.add(s);
		else
			throw new DuplicateSymbol();
	}
	
	/**
	 * Zwraca referencję do symbolu, który jest opisany znakiem podanym w parametrze.
	 * @param c znak opisujący symbol
	 * @return referencja do symbolu, albo null, jeśli symbol nie występuje w alfabecie
	 */
	public T getSymbol(Character c) 
	{
		for(T symb: symbols)
			if(symb.getName() == c)
				return symb;
		return null;
	}
	
	public int size()
	{
		return symbols.size();
	}
	
	/**
	 * Sprawdza, czy dany symbol znajduje się w alfabecie
	 * @param c znak opisujący symbol do sprawdzenia.
	 * @return true, jeśli symbol jest w alfabecie; false w przeciwnym przypadku.
	 */
	public boolean containsSymbol(char c)
	{
		for(T symb: symbols)
			if(symb.getName() == c)
				return true;
		return false;
	}
	
	
	/**
	 * Zwraca wszystkie symbole w alfabecie
	 * @return Kolekcja symobli
	 */
	public List<T> symbols()
	{
		return symbols;
	}

	@Override
	public String toString()
	{
		String res = "";
		for(T symb: symbols)
			res += symb;
		return res;
	}

	@Override
	public Iterator<T> iterator()
	{
		return symbols.listIterator();
	}
	
	public ListIterator<T> reverseIterator()
	{
		return symbols.listIterator(symbols.size());
	}

}

class DuplicateSymbol extends Exception {
	private static final long serialVersionUID = 1L;}