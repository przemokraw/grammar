package gramatyka;

public class Main
{
	public static void main(String args[])
	{
		System.out.println("***************************************");
		ContextFreeGrammar g = new ContextFreeGrammar("ab", "AB", new String[][] {{"AA","b"},{"BB","a"}} );
		System.out.println(g.toString());
		System.out.println("Is regular? " + g.ifRegular());
		System.out.println("Is Chomsky? " + g.ifChomsky());
		System.out.println("Is Greibach? " + g.ifGreibach());
		
		System.out.println("\n * TEST toGreibach():\n");
		
		NormalChomskyGrammar gc = new NormalChomskyGrammar(g);
		NormalGreibachGrammar gg = gc.toGreibach();
		System.out.println(gg.toString());
		System.out.println("Is regular? " + gg.ifRegular());
		System.out.println("Is Chomsky? " + gg.ifChomsky());
		System.out.println("Is Greibach? " + gg.ifGreibach());
		System.out.println("***************************************");

		System.out.println("***************************************");
		g = new ContextFreeGrammar("ab", "AB", new String[][] {{"BA","a"},{"AB","BA","b"}} );
		System.out.println(g.toString());
		System.out.println("Is regular? " + g.ifRegular());
		System.out.println("Is Chomsky? " + g.ifChomsky());
		System.out.println("Is Greibach? " + g.ifGreibach());
		
		System.out.println("\n * TEST toGreibach():\n");
		
		gc = new NormalChomskyGrammar(g);
		gg = gc.toGreibach();
		System.out.println(gg.toString());
		System.out.println("Is regular? " + gg.ifRegular());
		System.out.println("Is Chomsky? " + gg.ifChomsky());
		System.out.println("Is Greibach? " + gg.ifGreibach());
		System.out.println("***************************************");
		System.out.println("***************************************");
		
		g = new ContextFreeGrammar("ab", "AB", new String[][] {{"AA","a"},{"BB","b"}} );
		System.out.println(g.toString());
		System.out.println("Is regular? " + g.ifRegular());
		System.out.println("Is Chomsky? " + g.ifChomsky());
		System.out.println("Is Greibach? " + g.ifGreibach());
		
		System.out.println("\n * TEST toGreibach():\n");
		
		gc = new NormalChomskyGrammar(g);
		gg = gc.toGreibach();
		System.out.println(gg.toString());
		System.out.println("Is regular? " + gg.ifRegular());
		System.out.println("Is Chomsky? " + gg.ifChomsky());
		System.out.println("Is Greibach? " + gg.ifGreibach());
		System.out.println("***************************************");
		System.out.println("***************************************");		
		g = new ContextFreeGrammar("ab", "SAB", new String[][] {{"a"},{"b"},{"b"}});
		System.out.println(g.toString());
		System.out.println("Is regular? " + g.ifRegular());
		System.out.println("Is Chomsky? " + g.ifChomsky());
		System.out.println("Is Greibach? " + g.ifGreibach());
		
		System.out.println("\n * TEST toGreibach():\n");
		
		gc = new NormalChomskyGrammar(g);
		gg = gc.toGreibach();
		System.out.println(gg.toString());
		System.out.println("Is regular? " + gg.ifRegular());
		System.out.println("Is Chomsky? " + gg.ifChomsky());
		System.out.println("Is Greibach? " + gg.ifGreibach());
		
		System.out.println("\n * TEST bezkontekstowa -> regularna():\n");
		
		RegularGrammar gr = new RegularGrammar(g);
		System.out.println(gr.toString());
		System.out.println("Is regular? " + gr.ifRegular());
		System.out.println("Is Chomsky? " + gr.ifChomsky());
		System.out.println("Is Greibach? " + gr.ifGreibach());
		System.out.println("***************************************");
		System.out.println("***************************************");
		g = new ContextFreeGrammar("ab", "SAB", new String[][] {{"aB", "bA"},{"bA","a"},{"aB","b"}});
		System.out.println(g.toString());
		System.out.println("Is regular? " + g.ifRegular());
		System.out.println("Is Chomsky? " + g.ifChomsky());
		System.out.println("Is Greibach? " + g.ifGreibach());
		
		System.out.println("\n * TEST bezkontekstowa -> regularna():\n");
		
		gr = new RegularGrammar(g);
		System.out.println(gr.toString());
		System.out.println("Is regular? " + gr.ifRegular());
		System.out.println("Is Chomsky? " + gr.ifChomsky());
		System.out.println("Is Greibach? " + gr.ifGreibach());
		System.out.println("***************************************");
		System.out.println("***************************************");
		g = new ContextFreeGrammar("ab", "SAB", new String[][] {{"aB", "bA"},{"bA","a"},{"aB",""}});
		System.out.println(g.toString());
		System.out.println("Is regular? " + g.ifRegular());
		System.out.println("Is Chomsky? " + g.ifChomsky());
		System.out.println("Is Greibach? " + g.ifGreibach());
		System.out.println("***************************************");
		/* test: wypisanie symbolu pustego */
		System.out.println("***************************************");				
		g = new ContextFreeGrammar("ab", "SAB", new String[][] {{""},{"b"},{"b"}});
		System.out.println(g.toString());
		System.out.println("Is regular? " + g.ifRegular());
		System.out.println("Is Chomsky? " + g.ifChomsky());
		System.out.println("Is Greibach? " + g.ifGreibach());
		System.out.println("***************************************");
		/* test: toGreibach() wywołane na gramatyce, która ma za dużo nieterminali */
		System.out.println("***************************************aa");
		g = new ContextFreeGrammar("ab", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", new String[][] {{"AA","a"},{"BB","b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"},{"b"}});
		System.out.println(g.toString());
		System.out.println("Is regular? " + g.ifRegular());
		System.out.println("Is Chomsky? " + g.ifChomsky());
		System.out.println("Is Greibach? " + g.ifGreibach());
		
		System.out.println("\n * TEST toGreibach():\n");
		
		gc = new NormalChomskyGrammar(g);
		gg = gc.toGreibach();
		System.out.println(gg.toString());
		System.out.println("Is regular? " + gg.ifRegular());
		System.out.println("Is Chomsky? " + gg.ifChomsky());
		System.out.println("Is Greibach? " + gg.ifGreibach());
		System.out.println("***************************************");
		/* test: zduplikowanie nieterminala */
		System.out.println("***************************************");	
		g = new ContextFreeGrammar("ab", "SAAB", new String[][] {{""},{"b"},{"b"}});
		System.out.println(g.toString());
		System.out.println("Is regular? " + g.ifRegular());
		System.out.println("Is Chomsky? " + g.ifChomsky());
		System.out.println("Is Greibach? " + g.ifGreibach());
		System.out.println("***************************************");
		/* test: zduplikowanie terminala */
		System.out.println("***************************************");	
		g = new ContextFreeGrammar("abb", "SAB", new String[][] {{""},{"b"},{"b"}});
		System.out.println(g.toString());
		System.out.println("Is regular? " + g.ifRegular());
		System.out.println("Is Chomsky? " + g.ifChomsky());
		System.out.println("Is Greibach? " + g.ifGreibach());
		System.out.println("***************************************");

		

	}
}
