package gramatyka;

/** 
 * Abstrakcyjna klasa opisująca pojedynczy symbol gramatyki.
 * Każdy symbol może być albo terminalem, albo nieterminalem.
 * Po tej klasie będą dziedziczyły klasy opisujące terminale i nieterminale.
 * @author przemek
 * */
public abstract class Symbol
{
	
	private final char name;
	
	/**
	 * Konstruktor.
	 * @param name znak opisujący dany symbol
	 */
	public Symbol(char name){
		this.name = name;
	}
	
	/**
	 * Getter do opisu symbolu
	 * @return znak opisujący dany symbol
	 */
	public char getName()
	{
		return name;
	}
	
	/**
	 * Informacja o typie danego symbolu
	 * @return true, gdy Symbol jest terminalem, false w przeciwnym przypadku
	 */
	public abstract boolean isTerminal();
	
	
	@Override
	public String toString()
	{
		return Character.toString(name);
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null)
			return false;
		else if(obj instanceof Symbol)
			return name == ((Symbol) obj).name;
		else
			return false;
	}
}


class ForbiddenChar extends Exception{
	private static final long serialVersionUID = 1L;}